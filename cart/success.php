<?php
require_once "CarritoCompras.php";


?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title>Beacon Link</title>
    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="assets/sticky-footer-navbar.css" rel="stylesheet">
    <link href="assets/cart.css" rel="stylesheet">
    <link href="assets/main.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <script src="https://js.stripe.com/v3/"></script>
</head>
<body >
    <!-- Navigation-->
    <header> 
        <!-- Fixed navbar -->
        <nav style="" class="mainNav" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="#"> <img src="image/logo.png" class="logo"></a>
                  
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                
                </div>
            </div>
      </nav>
     
    </header> 

<!-- Begin page content -->

<div class="container" >

  <div class="row">
        <div class="col-12 col-md-12" > 
      <!-- Contenido -->
            <div class="limiter">
                <div class="container-login100">
                    <div class="wrap-login100">
                        <form class="login100-form validate-form">
                            <span class="login100-form-title p-b-26">
                               Success!
                            </span>
                            <span class="login100-form-title p-b-48">
                                <i class="zmdi zmdi-font"></i>
                            </span>

                           <div style="text-align: center;">
                            <p>Payment order was successful
                            </p>
                            
                            <a href="../index.php">Back to main page</a>
                           </div>

                            
                        </form>
                    </div>
                </div>
            </div>
	    
        </div>
 
      <!-- Fin Contenido --> 
    </div>
  </div>
  <!-- Fin row --> 

  
</div>
<!-- Fin container -->
<footer class="footer py-5">
		
        <div class="container py-xl-4 py-lg-3">
           <div class="address row mb-4">
              <div class="col-lg-4 address-grid">
                 <div class="row address-info">
                  <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
                    <i class="fa fa-envelope"></i>
                  </div>
                  <div class="col-md-9 col-8 address-right">
                    <p>
                      <a href="mailto:info@beacon-link.com" class="text-light"> info@beacon-link.com</a>
                    </p>
                  </div>
    
                </div>
              </div>
              <div class="col-lg-4 address-grid my-lg-0 my-4">
                <div class="row address-info">
                  <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
                    <i class="fa fa-phone"></i>
                  </div>
                  <div class="col-md-9 col-8 address-right">
                    <p class="text-light">Language Department<br>
                    (470) 315-4949 EXT 301<br>
                    Media Department <br>
                    (470) 315-4949 EXT 302<br>
                    Toll-Free<br>
                    (844) 706-7388</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 address-grid">
                <div class="row address-info">
                <div class="col-md-2 col-3 address-left text-lg-center text-sm-right text-center">
                      <i class="fa fa-map"></i>
                    </div>
                    <div class="col-md-10 col-9 address-right">
                      <p class="text-light">1755 North Brown Road. Suite 200<br>Lawrenceville, GA 30043</p>
                    </div>
                </div>
              </div>
            </div>
            <!-- social icons footer -->
            <div class="w3l-footer text-center pt-lg-4 mt-5">
              <ul class="list-unstyled">
                <li>
                  <a href="https://www.facebook.com/BeaconLink-266797260844967/" target="_blank">
                    <img src="image/fb.png" alt="">
                  </a>
                </li>
                <li class="mx-1">
                  <a href="https://www.instagram.com/beaconlinkcomm/" target="_blank">
                    <img src="image/ig.png" alt="">
                  </a>
                </li>
                <li class="mx-1">
                  <a href="https://www.youtube.com/channel/UCfGBUWRuLztitQ_ko27YmoA" target="_blank">
                    <img src="image/you.png" alt="">
                  </a>
                </li>
              </ul>
            </div>
            <!-- copyright -->
            <p style="text-align: right;" class="terms" ><a class="inline cboxElement" href="#inline_content">Terms and Conditions</a></p>
            <p style="text-align: left;" class="copy-right-grids text-light mt-4">© 2020 Beacon Link LLC. All Rights Reserved</p>
            <!-- //copyright -->
          </div>
        
        
      
    </footer>
    
<script src="assets/jquery-1.12.4-jquery.min.js"></script> 

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 

<script src="dist/js/bootstrap.min.js"></script>

</body>
</html>