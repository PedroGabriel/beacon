<!DOCTYPE html>
<html lang="es">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="img/bl.png">

  <!--Boostrap-css-->
  <link rel="stylesheet" href="css/animation-aos.css">
  <link href="css/aos.css" rel="stylesheet prefetch" type="text/css" media="all">
  <!-- Animation-->
  <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
  <!--Style css-->
  <link rel="stylesheet" href="css/font-awesome.css">
  <link rel="stylesheet" href="css/main.css" type="text/css" media="all">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <title>Beacon Link</title>
  <!-- Font Awesome Icons-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- Google Fonts-->
  <link rel="dns-prefetch" href="//www.google.com">
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS-->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap-->
  <link href="scss/navbar.scss" rel="stylesheet">
  <link href="scss/style.less" rel="stylesheet">
  <link href="css/creative.min.css" rel="stylesheet"> 
  <link href="cart/assets/sticky-footer-navbar.css" rel="stylesheet">
  <link href="cart/assets/cart.css" rel="stylesheet">


  <!--Video modal-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  <script src="js/scripts.js"></script>

  <!--Carousel-->
  <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <script src="https://js.stripe.com/v3/"></script>
  <style>
  /* Make the image fully responsive */
  .carousel-inner img {
      width: 100%;
      height: 100%;
  }

  .text-goverment{
    font-size:14px;
    color:white;
  }
  
  .section-bg {
  background: #ecf5ff;
}

/* Services Section
--------------------------------*/

#goverment {
  padding: 60px 0 40px 0;
  box-shadow: inset 0px 0px 12px 0px rgba(0, 0, 0, 0.1);
}

#goverment .box {
  padding: 30px;
  position: relative;
  overflow: hidden;
  border-radius: 10px;
  margin: 0 10px 40px 10px;
  background: #fff;
  box-shadow: 0 10px 29px 0 rgba(68, 88, 144, 0.1);
  transition: all 0.3s ease-in-out;
}

#goverment .box:hover {
  -webkit-transform: translateY(-5px);
  transform: translateY(-5px);
}

#goverment .icon {
  position: absolute;
  left: -10px;
  top: calc(50% - 32px);
}

#goverment .icon i {
  font-size: 64px;
  line-height: 1;
  transition: 0.5s;
}

#goverment .title {
  
  font-weight: 700;
  font-size: 18px;
}

#goverment .title2 {
  margin-left: 40px;
  font-weight: 700;
  margin-bottom: 15px;
  font-size: 14px;
}

#goverment .title a {
  color: #111;
}

#goverment .box:hover .title a {
  color: #007bff;
}

#goverment .description {
  font-size: 14px;
  margin-left: 40px;
  line-height: 24px;
  margin-bottom: 0;
}

#why-us {
  padding: 60px 0;
  background: #004a99;
}

#why-us .section-header h3,
#why-us .section-header p {
  color: #fff;
}

#why-us .card {
  background: #00458f;
  border-color: #00458f;
  border-radius: 10px;
  margin: 0 15px;
  padding: 15px 0;
  text-align: center;
  color: #fff;
  transition: 0.3s ease-in-out;
  height: 100%;
}

#why-us .card:hover {
  background: #003b7a;
  border-color: #003b7a;
}

#why-us .card i {
  font-size: 48px;
  padding-top: 15px;
  color: #bfddfe;
}

#why-us .card h5 {
  font-size: 22px;
  font-weight: 600;
}

#why-us .card p {
  font-size: 15px;
  color: #d8eafe;
}

#why-us .card .readmore {
  color: #fff;
  font-weight: 600;
  display: inline-block;
  transition: 0.3s ease-in-out;
  border-bottom: #00458f solid 2px;
}

#why-us .card .readmore:hover {
  border-bottom: #fff solid 2px;
}

#why-us .counters {
  padding-top: 40px;
}

#why-us .counters span {
  font-family: "Montserrat", sans-serif;
  font-weight: bold;
  font-size: 48px;
  display: block;
  color: #fff;
}

#why-us .counters p {
  padding: 0;
  margin: 0 0 20px 0;
  font-family: "Montserrat", sans-serif;
  font-size: 14px;
  color: #cce5ff;
}

/* Sections Header
--------------------------------*/

.section-header h3 {
  font-size: 36px;
  color: #283d50;
  text-align: center;
  font-weight: 500;
  position: relative;
}

.section-header h3 {
  font-size: 36px;
  color: #283d50;
  text-align: center;
  font-weight: 500;
  position: relative;
}

.box p {
  text-align: left;
  font-size: 15px;
  color: black;
  
}



</style>
		



</head>

<body id="page-top">

  <!--Preloader-->
  <div class="preloader">
    <div class="inner">
        <div class="item item1"></div>
        <div class="item item2"></div>
        <div class="item item3"></div>
    </div>
    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
</div>

  	<!-- Navigation -->
	<nav  class="navbar navbar-expand-lg navbar-light fixed-top py-3" style="background-image: linear-gradient(to right bottom, #0f5e92, #23acf3);">
		<div class="container">
		<a class="navbar-brand"  href="index.php"><img src="img/logo.png" class="logo"></a>
          <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto my-2 my-lg-0 align-content-end" >
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger"  href="index.php">Home</a>
                </li>
                
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" target="_blank" href="https://hermes.beacon-link.com/login">Sign In</a>
                </li>
                
              </ul>
            </div>
		</div>
	</nav>


	<!-- Content -->
	<div id="content" class="site-content" style="margin-top:200px;margin-bottom:150px;">
		
		<div class="col-12 text-center text-justify" style="margin-bottom:50px;color: #0f5e92;font-size: 3em;animation: spin 2s linear infinite;">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
            </svg>
            <p>Success!</p>
		</div>
		
		<div class="section-training col-md-10 offset-1 row">
			<div class="col-md-12 col-sm-12">
                <div class="limiter">
                    <div class="container-login100">
                        <div class="wrap-login100">
                            <form class="login100-form validate-form">
                               
                                <span class="login100-form-title p-b-48">
                                    <i class="zmdi zmdi-font"></i>
                                </span>

                            <div style="text-align: center;">
                                <p style="font-size:1.25rem;">Payment order was successful</p>
                                
                                <a href="index.php">Back to main page</a>
                            </div>

                                
                            </form>
                        </div>
                    </div>
                </div>
			</div>
			
		</div>	
	</div>	
      
	<!-- Footer -->
	<footer class="footer py-5">
			
		<div class="container py-xl-4 py-lg-3">
		<div class="address row mb-4">
			<div class="col-lg-4 address-grid">
				<div class="row address-info">
				<div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
					<i class="fa fa-envelope"></i>
				</div>
				<div class="col-md-9 col-8 address-right">
					<p>
					<a href="mailto:info@beacon-link.com" class="text-light"> info@beacon-link.com</a>
					</p>
				</div>

				</div>
			</div>
			<div class="col-lg-4 address-grid my-lg-0 my-4">
				<div class="row address-info">
				<div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
					<i class="fa fa-phone"></i>
				</div>
				<div class="col-md-9 col-8 address-right">
					<p class="text-light">Language Department<br>
					(470) 315-4949 EXT 301<br>
					Media Department <br>
					(470) 315-4949 EXT 302<br>
					Toll-Free<br>
					(844) 706-7388</p>
				</div>
				</div>
			</div>
			<div class="col-lg-4 address-grid">
				<div class="row address-info">
				  <div class="col-md-2 col-3 address-left text-lg-center text-sm-right text-center">
            <i class="fa fa-map"></i>
          </div>
          <div class="col-md-10 col-9 address-right">
            <p class="text-light">1755 North Brown Road. Suite 200<br>Lawrenceville, GA 30043</p>
          </div>
				</div>
			</div>
			</div>
			<!-- social icons footer -->
			<div class="w3l-footer text-center pt-lg-4 mt-5">
			<ul class="list-unstyled">
				<li>
				<a href="https://www.facebook.com/BeaconLink-266797260844967/" target="_blank">
					<img src="img/fb.png" alt="">
				</a>
				</li>
				<li class="mx-1">
				<a href="https://www.instagram.com/beaconlinkcomm/" target="_blank">
					<img src="img/ig.png" alt="">
				</a>
				</li>
				<li class="mx-1">
				<a href="https://www.youtube.com/channel/UCfGBUWRuLztitQ_ko27YmoA" target="_blank">
					<img src="img/you.png" alt="">
				</a>
				</li>
			</ul>
			</div>
			<!-- copyright -->
			<p style="text-align: right;" class="terms" ><a class="inline cboxElement" href="#inline_content">Terms and Conditions</a></p>
			<p style="text-align: left;" class="copy-right-grids text-light mt-4">© 2020 Beacon Link LLC. All Rights Reserved</p>
			<!-- //copyright -->
		</div>
		
		
	
	</footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Custom scripts for this template -->


  
  <!-- Js files -->

	<script src="js/bootstrap.js"></script>
	<!-- Necessary-JavaScript-File-For-Bootstrap -->

	<!-- dropdown smooth -->
	
	<!-- //dropdown smooth -->

	<!-- stats -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.countup.js"></script>
  
</body>

</html>
