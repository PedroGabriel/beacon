<style>
#po_number{
  
   display:none
}
#po_number2{
  
  display:none
}
</style>

<section id="form1"  name="form1" method="post" class="wrap">
    
    <div class="content">
    <!-- Main content -->
        <section class="content" id="content-form-usuario">
            <div class="row">
                <section class="col-xs-12 col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div>
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item" style="background-color: white; color: white;">
                                        <a id="one-tab" class="nav-link" data-toggle="tab" href="#seccion-1" role="tab" aria-controls="false">Request a quote</a>
                                    </li>
                                    <li class="nav-item" style="background-color: white; color: white;">
                                        <a id="two-tab" class="nav-link" href="#seccion-2"  data-toggle="tab" role="tab" aria-controls="false"><i class="fa fa-document"></i>Request a Translation</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                <div id="seccion-1" class="tab-pane active" role="tabpanel">
                                      <form method="post" class="contact_form" id="formulario" name="formulario" role="form" action="mail/document1.php" enctype="multipart/form-data">
                                            <div class="form-group mt-4">
                                                <!--Tipo de servicio-->
                                                <div class="col-xs-12 col-md-12">
                                                    <div>
                                                        <h4 class="text-center text-justify"  style=" background-color: #0407c9; color: white;  visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">Document Translation: I like to get a quote</h4> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mt-4">
                                                <!--customer-->
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-4"></div>
                                                        <div class="col-xs-12 col-md-4">
                                                              <div class="form-group" style="text-align: center;">
                                                                  <h4><label class="control-label" for="record">Are you an existing customer?</label></h4>
                                                                      <select name="TypeCustomer" id="TypeCustomer" onchange="ShowSelected();" style="padding: 10px; background-color: white; color: rgba(0, 0, 0, 0.555);" required>
                                                                          <option >Select Option</option>
                                                                          <option value="Yes">Yes</option>
                                                                          <option value="No">No</option>
                                                                      </select>
                                                              </div> 
                                                          </div>
                                                          <div class="col-xs-12 col-md-4"></div>
                                                    </div>
                                                </div>
                                            </div>

                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-xs-12 col-md-4" id="po_number">
                                                                    <div class="form-group" >
                                                                      <div class="input-group-addon"><i class="">PO Number</i></div>
                                                                      <input type="text" id="dpo_number" name="dpo_number" class="form-control input-sm valida-texto"  required/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Name</i></div>
                                                                        <input type="text" id="Name" name="Name" class="form-control input-sm valida-texto"  required/>
                                                                        <input type="hidden" id="service_requested" name="service_requested" class="form-control input-sm valida-texto"  value="Document Translation"/>
                                                                      </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Last Name</i></div>
                                                                        <input type="text" id="Last" name="Last" class="form-control input-sm valida-texto"  required/>
                                                                      </div>
                                                                  </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-xs-12 col-md-4">
                                                                    <div class="form-group" >
                                                                      <div class="input-group-addon"><i class="">Contact Number</i></div>
                                                                      <input type="text" id="ContactNumber" name="ContactNumber" class="form-control valida-telefono" data-inputmask='"mask": "(9999) 999-9999"' data-mask />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Ext</i></div>
                                                                      <input type="text" id="Ext" name="ExtPhone" class="form-control valida-telefono"  />
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Email</i></div>
                                                                      <input type="email" id="Email" name="Email" class="form-control" placeholder="email@gmail.com"  title=" '@'' (.)" required>
                                                                      </div>
                                                                  </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-xs-12 col-md-3">
                                                                  <div class="form-group" >
                                                                    <div class="input-group-addon"><i class="">Company Name</i></div>
                                                                    <input type="text" name="Company" class="form-control input-group valida-texto" placeholder="" required/>
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                <div class="form-group">
                                                                    <div class="input-group-addon"><i class="">In what language is the original document?</i></div>
                                                                    <input type="text"  name="LenguageDocument" class="form-control input-sm valida-texto" placeholder="" required/>
                                                                      
                                                                    </div>
                                                                </div>
                                                                
                                                                  <div class="col-xs-12 col-md-5">
                                                                    <div class="form-group">
                                                                          <div class="input-group-addon"><i class="">Into what Language you need the translation to be done?</i></div>
                                                                          <input type="text"  name="DocumentTranslation" class="form-control input-sm valida-texto" placeholder="" required/>
                                                                        </div>
                                                                    </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                             
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-4">
                                                              <div class="form-group" >
                                                                <div class="input-group-addon"><i class="">When do you need the translation back to you?</i></div>
                                                                <input type="date"  name="DateTranslation" style="text-align: center;" class="form-control " placeholder="" required/>
                                                              </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4">
                                                              <div class="form-group">
                                                                  <div class="input-group-addon"><i class=""></i></div>
                                                                  <input type='file' name='archivo2' id='archivo2' placeholder="carga tu boucher" required>
                                                                    
                                                                  </div>
                                                              </div>
                                                              <div class="col-xs-12 col-md-4">
                                                                    <div class="form-group">
                                                                        <div class="input-group-addon"><i class="">Billing Address</i></div>
                                                                        <input type="text" id="billing_address" name="billing_address" class="form-control input-sm valida-texto" required/>
                                                                        </div>
                                                                    </div>
                                                              </div>
                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                             
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-4">
                                                              <div class="form-group" >
                                                                <div class="input-group-addon"><i class="">City</i></div>
                                                                <input type="text" id="city_address" name="city_address" class="form-control input-sm valida-texto" required/>
                                                              </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4">
                                                              <div class="form-group">
                                                                  <div class="input-group-addon"><i class="">State</i></div>
                                                                  <input type="text" id="state_address" name="state_address" class="form-control input-sm valida-texto" required/>
                                                                    
                                                                  </div>
                                                              </div>
                                                              <div class="col-xs-12 col-md-4">
                                                                    <div class="form-group">
                                                                        <div class="input-group-addon"><i class="">Zipcode</i></div>
                                                                        <input type="text" id="zipcode_address" name="zipcode_address" class="form-control input-sm valida-texto" required/>
                                                                      </div>
                                                                </div>
                                                              
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group mt-4">
                                                <!--send-->
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-4"></div>
                                                            <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group" style="text-align: center;">
                                                                  <input class="btn btn-primary" type="submit" value="SEND" style="color:white; background-color: #5d5dff; border-color: #0407c9; font-size: 15px;  padding: 15px;" placeholder="   SEND   "> 
                                                                  </div> 
                                                              </div>
                                                              <div class="col-xs-12 col-md-4"></div>
                                                        </div>
                                                    </div>
                                                </div>
             

                                       </form>
                                </div>
                                <div id="seccion-2" class="tab-pane" role="tabpanel">
                                      <form method="post" class="contact_form" id="formulario" name="formulario" role="form" action="mail/document1.php" enctype="multipart/form-data">
                                            <div class="form-group mt-4">
                                                <!--Tipo de servicio-->
                                                <div class="col-xs-12 col-md-12">
                                                    <div>
                                                        <h4 class="text-center text-justify"  style=" background-color: #0407c9; color: white;  visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">Document Translation: Request a service</h4> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mt-4">
                                                <!--customer-->
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-4"></div>
                                                        <div class="col-xs-12 col-md-4">
                                                              <div class="form-group" style="text-align: center;">
                                                                  <h4><label class="control-label" for="record">Are you an existing customer?</label></h4>
                                                                      <select name="sr_customer" id="sr_customer" onchange="ShowSelected2();" style="padding: 10px; background-color: white; color: rgba(0, 0, 0, 0.555);" required>
                                                                          <option >Select Option</option>
                                                                          <option value="Yes">Yes</option>
                                                                          <option value="No">No</option>
                                                                      </select>
                                                              </div> 
                                                          </div>
                                                          <div class="col-xs-12 col-md-4"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-md-4" id="po_number2">
                                                                    <div class="form-group" >
                                                                      <div class="input-group-addon"><i class="">PO Number</i></div>
                                                                      <input type="text" id="sr_po_number" name="sr_po_number" class="form-control input-sm valida-texto"  required/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                        <div class="input-group-addon"><i class="">Name</i></div>
                                                                        <input type="text" id="sr_first_name" name="sr_first_name" class="form-control input-sm valida-texto"  required/>
                                                                        <input type="hidden" id="service_requested" name="service_requested" value="Document Translation" class="form-control input-sm valida-texto"/>
                                                                      </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Last Name</i></div>
                                                                      <input type="text" id="sr_last_name" name="sr_last_name" class="form-control input-sm valida-texto"  required/>
                                                                      </div>
                                                                  </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group" >
                                                                      <div class="input-group-addon"><i class="">Contact Number</i></div>
                                                                      <input type="text" id="sr_contact_number" name="sr_contact_number" class="form-control valida-telefono" data-inputmask='"mask": "(9999) 999-9999"' data-mask />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Ext</i></div>
                                                                      <input type="text" id="sr_exp_phone" name="sr_exp_phone" class="form-control valida-telefono"  />
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Email</i></div>
                                                                      <input type="email" id="sr_email" name="sr_email" class="form-control" placeholder="email@gmail.com"  title=" '@'' (.)" required>
                                                                      </div>
                                                                  </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-md-3">
                                                                  <div class="form-group" >
                                                                    <div class="input-group-addon"><i class="">Company Name</i></div>
                                                                    <input type="text" id="sr_company" name="sr_company" class="form-control input-group valida-texto" placeholder="" required/>
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                <div class="form-group">
                                                                    <div class="input-group-addon"><i class="">In what language is the original document?</i></div>
                                                                    <input type="text" id="sr_lenguaje_document" name="sr_lenguaje_document" class="form-control input-sm valida-texto" placeholder="" required/>
                                                                      
                                                                    </div>
                                                                </div>
                                                                
                                                                  <div class="col-5">
                                                                    <div class="form-group">
                                                                          <div class="input-group-addon"><i class="">Into what Language you need the translation to be done?</i></div>
                                                                          <input type="text" id="sr_translation_document" name="sr_translation_document" class="form-control input-sm valida-texto" placeholder="" required/>
                                                                        </div>
                                                                    </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                             
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-4">
                                                              <div class="form-group" >
                                                                <div class="input-group-addon"><i class="">City</i></div>
                                                                <input type="text" id="sr_city" name="sr_city" class="form-control input-sm valida-texto" required/>
                                                              </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4">
                                                              <div class="form-group">
                                                                  <div class="input-group-addon"><i class="">State</i></div>
                                                                  <input type="text" id="sr_state" name="sr_state" class="form-control input-sm valida-texto" required/>
                                                                    
                                                                  </div>
                                                              </div>
                                                              <div class="col-xs-12 col-md-4">
                                                                    <div class="form-group">
                                                                        <div class="input-group-addon"><i class="">Zipcode</i></div>
                                                                        <input type="text" id="sr_zipcode" name="sr_zipcode" class="form-control input-sm valida-texto" required/>
                                                                      </div>
                                                                </div>
                                                              
                                                        </div>
                                                    </div>
                                                </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                             
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-4">
                                                              <div class="form-group" >
                                                                <div class="input-group-addon"><i class="">Date</i></div>
                                                                <input type="date" id="sr_date" name="sr_date" style="text-align: center;" class="form-control " placeholder="" required/>
                                                              </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4">
                                                              <div class="form-group">
                                                                  <div class="input-group-addon" style="background-color: white;"><i class="">If your are a new customer please provide us with billing address:</i>
                                                                  </div>
                                                                  <textarea id="sr_billing_address" name="sr_billing_address"></textarea>
                                                                    
                                                                  </div>
                                                              </div>
                                                              <div class="col-xs-12 col-md-4">
                                                                    <div class="form-group">
                                                                        <div class="input-group-addon"><i class="">Charge your Bauche</i></div>
                                                                        <input type='file' name="sr_archivo" id="sr_archivo" placeholder="carga tu boucher" required>
                                                                        </div>
                                                                    </div>
                                                              </div>
                                                    </div>
                                              </div>
                                              
                                                <div class="form-group mt-4">
                                                <!--send-->
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-4"></div>
                                                            <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group" style="text-align: center;">
                                                                  <input class="btn btn-primary" type="submit" value="SEND" style="color:white; background-color: #5d5dff; border-color: #0407c9; font-size: 15px;  padding: 15px;" placeholder="   SEND   "> 
                                                                  </div> 
                                                              </div>
                                                              <div class="col-xs-12 col-md-4"></div>
                                                        </div>
                                                    </div>
                                                </div>
             
                                       </form>
                                </div>
                              </div>
                               
                          </div>
                        </div>              
                    </section>         
                </div>
          </section>
      </div>
</section>
<script type="text/javascript">
function ShowSelected()
{
/* Para obtener el valor */
var cod = document.getElementById("TypeCustomer").value;
//alert(cod);
 
/* Para obtener el texto */
var combo = document.getElementById("TypeCustomer");
var selected = combo.options[combo.selectedIndex].text;
//alert(selected);

if(cod == 'Yes'){
  $('#po_number').show(1000);
}


}

function ShowSelected2()
{
/* Para obtener el valor */
var cod = document.getElementById("sr_customer").value;
//alert(cod);
 
/* Para obtener el texto */
var combo = document.getElementById("sr_customer");
var selected = combo.options[combo.selectedIndex].text;
//alert(selected);
if(cod == 'Yes'){
  $('#po_number2').show(1000);
}



}
</script>