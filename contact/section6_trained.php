<div id="02" class="tab-pane" role="tabpanel">

  <!--mensagge--> 
    <div class="form-group">
      
          <div col-xs-12 col-md-8>
            <div class="form-group" style="text-align: center;">
                <h4><label class="control-label" for="record">Excellent, Lets complete your aplication</label></h4>
            </div>  
          </div>
    </div>
    <!--Tipo de servicio-->
     <div class="col-xs-12 col-md-12">
        <div class="form-group">
          <h4 class="text-center text-justify"  style=" background-color: #0407c9; color: white;  visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">Trained</h4> 
        </div>
      </div>

      <form method="post" class="contact_form" id="formulario" name="formulario" role="form" action="mail/trained.php" enctype="multipart/form-data">

                  <div class="form-group"> <!--Po number, name last name-->
                        <div class="col-xs-12 col-md-12">
                            <div class="row">
                                      <div class="col-xs-12 col-md-4">
                                            <div class="form-group" >
                                                <div class="input-group-addon"><i class="">Prefix (Mr. Mrs., etc.)</i></div>
                                                <input type="text" id="tra_prefix" name="tra_prefix" class="form-control valida-prefix"  />
                                            </div>
                                      </div>
                                      <div class="col-xs-12 col-md-4">
                                              <div class="form-group">
                                                    <div class="input-group-addon"><i class="">Name</i></div>
                                                    <input type="text" id="tra_name" name="tra_name" class="form-control valida-tName"  />
                                              </div>
                                        </div>

                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group">
                                                <div class="input-group-addon"><i class="">Last Name</i></div>
                                                    <input type="text" id="tra_last_name" name="tra_last_name" class="form-control input-sm valida-texto"  required/>
                                                </div>
                                        </div>

                              </div>
                            
                          
                    </div>
                  </div>

                  <div class="form-group"> <!--Po number, name last name-->
                        <div class="col-xs-12 col-md-12">
                            <div class="row">
                                      <div class="col-xs-12 col-md-6">
                                            <div class="form-group" >
                                                <div class="input-group-addon"><i class="">Gender</i></div>
                                                  <select  id="tra_gender" name="tra_gender" style="padding: 10px; background-color: white; color: rgba(0, 0, 0, 0.555);">
                                                                                  <option value="">Select Option</option>
                                                                                  <option value="Woman" name="Woman">Woman</option>
                                                                                  <option value="Man" name="Man">Man</option>
                                                                                  <option value="It doesn’t matter">It doesn’t matter</option>
                                                  </select>
                                            </div>
                                      </div>
                                      <div class="col-xs-12 col-md-6">
                                              <div class="form-group">
                                                    <div class="input-group-addon"><i class="">DBO</i></div>
                                                    <input type="date" id="tra_age" name="tra_age" class="form-control input-group valida-texto" placeholder="" required/>
                                              </div>
                                        </div>

                                      

                              </div>
                        </div>
                  </div>

                  <div class="form-group"> <!--Po number, name last name-->
                      <div class="col-xs-12 col-md-12">
                        <div class="row">
                                  <div class="col-md-12 text-center text-justify"><p style="color: #000000;">Complete Address:</p></div>
                                  <div class="col-xs-12 col-md-3">
                                        <div class="form-group" >
                                            <div class="input-group-addon"><i class="">Address</i></div>
                                            <input type="text" id="tra_address" name="tra_address" class="form-control valida-prefix"  />
                                        </div>
                                  </div>
                                  <div class="col-xs-12 col-md-3">
                                      <div class="form-group" >
                                              <div class="input-group-addon"><i class="">City</i></div>
                                                      <input type="text" id="city_address" name="city_address" class="form-control input-sm valida-texto" required/>
                                        </div>
                                </div>                            
                                <div class="col-xs-12 col-md-3">
                                    <div class="form-group">
                                          <div class="input-group-addon"><i class="">State</i></div>
                                              <input type="text" id="state_address" name="state_address" class="form-control input-sm valida-texto" required/>
                                                                        
                                          </div>
                                    </div>
                                <div class="col-xs-12 col-md-3">
                                    <div class="form-group">
                                        <div class="input-group-addon"><i class="">Zipcode</i></div>
                                        <input type="text" id="zipcode_address" name="zipcode_address" class="form-control input-sm valida-texto" required/>
                                  </div>
                                </div>

                            </div>
                        </div>
                    </div>
                  

                  <div class="form-group"> <!--Po number, name last name-->
                      <div class="col-xs-12 col-md-12">
                          <div class="row">
                  
                              <div class="col-xs-12 col-md-3">
                                    <div class="form-group" >
                                        <div class="input-group-addon"><i class="">Home Phone</i></div>
                                        <input type="text" id="tra_phone" name="tra_phone" class="form-control valida-telefono" data-inputmask='"mask": "(9999) 999-9999"' data-mask />
                                    </div>
                              </div>
                              <div class="col-xs-12 col-md-3">
                                  <div class="form-group" >
                                          <div class="input-group-addon"><i class="">Ext</i></div>
                                          <input type="text" id="tra_ext" name="tra_ext" class="form-control valida-telefono"  />
                                    </div>
                            </div>                            
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group">
                                      <div class="input-group-addon"><i class="">Fax</i></div>
                                      <input type="text" id="tra_fax" name="tra_fax" class="form-control valida-telefono"  />
                                      </div>
                                </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group">
                                    <div class="input-group-addon"><i class="">Email Address</i></div>
                                    <input type="email" id="tra_email" name="tra_email" class="form-control" placeholder="email@gmail.com"  title=" '@'' (.)" required>
                              </div>
                            </div>

                          </div>
                      </div>
                  </div>
            

                  
                  <div class="form-group"> <!--Po number, name last name-->
                        <div class="col-xs-12 col-md-12">
                            <div class="row">
                                      <div class="col-xs-12 col-md-4">
                                            <div class="form-group" >
                                                <div class="input-group-addon"><i class="">Primary Language</i></div>
                                                <input type="text" id="tra_primary" name="tra_primary" class="form-control input-group valida-texto" placeholder="" required/>
                                            </div>
                                      </div>
                                      <div class="col-xs-12 col-md-4">
                                              <div class="form-group">
                                                    <div class="input-group-addon"><i class="">Languages that you are able to interpret</i></div>
                                                    <textarea id="tra_lang" name="tra_lang"></textarea>
                                              </div>
                                        </div>

                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group">
                                                <div class="input-group-addon"><i class="">How Many years of school you completed?</i></div>
                                                    <input type="text" id="tra_school" name="tra_school" name="cert_last_name" class="form-control input-sm valida-texto"  required/>
                                                </div>
                                        </div>
                  
                              </div>
                    </div>
               </div>

              <div class="form-group"> <!--Po number, name last name-->
                      <div class="col-xs-12 col-md-12">
                          <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                          <div class="form-group" >
                                              <div class="input-group-addon"><i class="">Highest education degree?</i></div>
                                              <input type="text" id="tra_education" name="tra_education" class="form-control input-group valida-texto" placeholder="" required/>
                                          </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                            <div class="form-group">
                                                  <div class="input-group-addon"><i class="">Have you ever taken an interpreter training before?</i></div>
                                                  <select nname="tra_training" id="tra_training" style="padding: 10px; ">
                                                    <option value="Yes" name="Yes" id="Yes">Yes</option>
                                                    <option value="No" name="No" id="No">No</option>
                                                  </select>
                                            </div>
                                      </div>

                                      <div class="col-xs-12 col-md-4">
                                          <div class="form-group">
                                              <div class="input-group-addon"><i class="">What year you took your last 40-hours interpreter training?</i></div>
                                                  <input type="text" id="tra_last" name="tra_last" class="form-control input-sm valida-texto"  required/>
                                              </div>
                                      </div>
                
                            </div>
                   </div>
              </div>

              <div class="form-group"> <!--Po number, name last name-->
                <div class="col-xs-12 col-md-12">
                    <div class="row">
                              <div class="col-xs-12 col-md-4">
                                    <div class="form-group" >
                                        <div class="input-group-addon"><i class="">Through what organization or company, you took your training?</i></div>
                                        <input type="text" id="tra_company" name="tra_company" class="form-control input-group valida-texto" placeholder="" required/>
                                    </div>
                              </div>
                              <div class="col-xs-12 col-md-4">
                                      <div class="form-group">
                                            <div class="input-group-addon"><i class="">Do you have experience interpreting?</i></div>
                                            <select name="tra_experience" id="tra_experience"  style="padding: 10px; ">
                                              <option value="Yes" name="Yes" id="Yes">Yes</option>
                                              <option value="No" name="No" id="No">No</option>
                                            </select>
                                      </div>
                                </div>

                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <div class="input-group-addon"><i class="">If yes, explain (it does not matter if it was on informal settings like family, friends, etc.):</i></div>
                                        <textarea id="tra_notes" name="tra_notes"></textarea>
                                        </div>
                                </div>
          
                      </div>

            </div>
          </div>

          <div class="form-group"> <!--Po number, name last name-->
                        <div class="col-xs-12 col-md-12">
                            <div class="row">
                                      <div class="col-xs-12 col-md-6">
                                            <div class="form-group" >
                                                <div class="input-group-addon"><i class="">Resume <span>*</span></i></div>
                                                <input style="display:none; border-color: white;" type="hidden" name="Resume" id="Resume" value="">
                                                <input type='file'  name="tra_resumen" id="tra_resumen" placeholder="Upload" required>
                                            </div>
                                      </div>
                                      <div class="col-xs-12 col-md-6">
                                            <div class="form-group" >
                                                <div class="input-group-addon"><i class="">Training Certificate or Certification <span>*</span></i></div>
                                                <input style="display:none; border-color: white;" type="hidden" name="Training Certificate or Certification" id="Training Certificate or Certification" value="">
                                                <input type='file' name="tra_certificate" id="tra_certificate" placeholder="Upload" required>
                                            </div>
                                      </div>
                                      

                            </div>
                        </div>
                  </div>


              <div class="form-group mt-4"> <!--Po number, name last name-->
                        <div class="col-xs-12 col-md-12">
                              <div class="row">
                                      <div class="col-xs-12 col-md-4"></div>
                                      <div class="col-xs-12 col-md-4">
                                            <div class="form-group" >
                                              <input onclick="send1()" class="btn btn-primary" type="submit" value="SEND" style="color:white; background-color: #5d5dff; border-color: #0407c9; font-size: 15px;  padding: 15px;" placeholder="   SEND   "> 
                                            </div>
                                      </div>
                                      <div class="col-xs-12 col-md-4"></div>
                              </div>
                        </div>
                  </div>


      </form>
      
</div>

<script>
    function send3()
       {
        var mensaje;
        var opcion = confirm("Thanks for expressing interest in becoming part of our network of professionals. We will review your documentation anda credentials , once is done you will receive an e-mail with complete guidance to our portal and your login credentials, so you can complete your profile, enter your rates and complete the needed documentation");
        if (opcion == true) {
                  mensaje = "OK";
         } else {
                  mensaje = "Cancelar";
         }
         //document.getElementById("ejemplo").innerHTML = mensaje;
                          
        }
  </script> 