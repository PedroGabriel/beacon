<div class="tab-pane" id="tab5" role="tabpanel" aria-labelledby="five-tab" >
    <form  method="post" id="form-2" name="form-2" role="form" action="mail/training.php">
                                            <div class="form-group mt-4">
                                                <!--Tipo de servicio-->
                                                <div class="col-xs-12 col-md-12">
                                                    <div>
                                                        <h4 class="text-center text-justify"  style=" background-color: #0407c9; color: white;  visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">Interpreter Training</h4> 
                                                    </div>
                                                    <div class="text-center text-justify m-2">
                                                    <p class="card-text" style="color: #000;font-size: 18px;font-family: 'Times New Roman', Times, serif">We would like to give you a warm welcome;
                                                     just by visiting this part of our site, you are showing your willingness and eagerness to become a professional interpreter. Let me tell you that you are choosing a beautiful, fulfilling career, but if you are planning to get trained and you are a bilingual staff somewhere, you are adding a great skill that your employer will never regret having as part of the staff.</p>

                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="form-group mt-4"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                
                                                                <div class="col-xs-12 col-md-6">
                                                                  <div class="form-group">
                                                                        <div class="input-group-addon"><i class="">Name</i></div>
                                                                        <input type="text" id="tra_first_name" name="tra_first_name" class="form-control input-sm valida-texto" placeholder="name"  required/>
                                                                      </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-6">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Last Name</i></div>
                                                                      <input type="text" id="tra_last_name" name="tra_last_name" class="form-control input-sm valida-texto" placeholder="Last Name" required/>
                                                                      </div>
                                                                  </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="form-group" >
                                                                      <div class="input-group-addon"><i class="">Phone Number</i> </div>
                                                                      <input type="number" id="tra_phone_number" name="tra_phone_number" class="form-control input-sm valida-texto" placeholder="ej. 00-1-212-324-4152" required/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-xs-12 col-md-6">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class=""> Email</i></div>
                                                                      <input type="email" id="tra_email" name="tra_email" class="form-control" placeholder="email@gmail.com"  title=" '@'' (.)" required>
                                                                  </div>
                                                                </div>
       
                                                          </div>

                                                    </div>
                                              </div>
                                              <div class="form-group mt-4">
                                                <!--customer-->
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-4"></div>
                                                        <div class="col-xs-12 col-md-4">
                                                              <div class="form-group" style="text-align: center;">
                                                                  <h4><label class="control-label" for="record">Choose below the training date you wish to enroll:</label></h4>
                                                                  <select name="training_date" id="training_date" style="padding: 10px;">
                                                                    
                                                                    <option value="MODESTO, CA -  Aug 22, 23, 28,  2020">MODESTO, CA -  Aug 22, 23, 28,  2020</option>
                                                                    <option value="ATLANTA, GA - 29 & 30,  2020" >ATLANTA, GA - 29 & 30,  2020</option>
                                                                  </select>
                                                              </div> 
                                                          </div>
                                                          <div class="col-xs-12 col-md-4"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            

                                                <div class="form-group mt-4">
                                                <!--send-->
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-4"></div>
                                                            <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group" style="text-align: center;">
                                                                  <input class="btn btn-primary" type="submit" value="SEND" style="color:white; background-color: #5d5dff; border-color: #0407c9; font-size: 15px;  padding: 15px;" placeholder="   SEND   "> 
                                                                  </div> 
                                                              </div>
                                                              <div class="col-xs-12 col-md-4"></div>
                                                        </div>
                                                    </div>
                                                </div>
             
                                       </form>
                                </div>