<style>

#po_number3{
  
  display:none
}
</style>



<div class="tab-pane active" id="tab2" role="tabpanel" aria-labelledby="two-tab">
    
    <div class="content">
    <!-- Main content -->
      <section id="form2"  name="form2" method="post" class="wrap">
            <div class="row">
                <section class="col-xs-12 col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div>
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item" style="background-color: white; color: white;">
                                      <a id="two-tab" class="nav-link" data-toggle="tab" href="#section-2" role="tab" ><i class="fa fa-document"></i>Request an Interpreter</a>
                                    </li>
                                    
                                </ul>
                                <div class="tab-content">
                                  <div class="tab-pane active" role="tabpanel"  id="section-2">
                                      <form  method="post" id="form-2" name="form-2" role="form" action="mail/on-site.php" enctype="multipart/form-data">
                                            <div class="form-group mt-4">
                                                <!--Tipo de servicio-->
                                                <div class="col-xs-12 col-md-12">
                                                    <div>
                                                        <h4 class="text-center text-justify"  style=" background-color: #0407c9; color: white;  visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">On-Site Interpretation: Request a Service</h4> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mt-4">
                                                <!--customer-->
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-4"></div>
                                                        <div class="col-xs-12 col-md-4">
                                                              <div class="form-group" style="text-align: center;">
                                                                  <h4><label class="control-label" for="record">Are you an existing customer?</label></h4>
                                                                      <select name="on_customer" id="on_customer" onchange="ShowSelected3();" style="padding: 10px; background-color: white; color: rgba(0, 0, 0, 0.555);" required>
                                                                          <option >Select Option</option>
                                                                          <option value="Yes">Yes</option>
                                                                          <option value="No">No</option>
                                                                      </select>
                                                              </div> 
                                                          </div>
                                                          <div class="col-xs-12 col-md-4"></div>
                                                    </div>
                                                </div>
                                            </div>

                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-xs-12 col-md-4" id="po_number3">
                                                                    <div class="form-group" >
                                                                      <div class="input-group-addon"><i class="">PO Number</i></div>
                                                                      <input type="text" id="on_po_number" name="on_po_number" class="form-control input-sm valida-texto"  />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Name</i></div>
                                                                        <input type="text" id="on_first_name" name="on_first_name" class="form-control input-sm valida-texto"  required/>
                                                                      </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Last Name</i></div>
                                                                      <input type="text" id="on_last_name" name="on_last_name" class="form-control input-sm valida-texto"  required/>
                                                                      </div>
                                                                  </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-xs-12 col-md-4">
                                                                    <div class="form-group" >
                                                                      <div class="input-group-addon"><i class="">Contact Number</i></div>
                                                                      <input type="number" id="on_contact_number" name="on_contact_number" class="form-control input-sm valida-texto"  required/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Ext</i></div>
                                                                      <input type="number" id="on_exp_phone" name="on_exp_phone" class="form-control input-sm valida-texto"  required/>
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Email</i></div>
                                                                      <input type="email" id="on_email" name="on_email" class="form-control" placeholder="email@gmail.com"  title=" '@'' (.)" required>
                                                                      </div>
                                                                  </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                             
                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-xs-12 col-md-3">
                                                                  <div class="form-group" >
                                                                    <div class="input-group-addon"><i class="">Company Name</i></div>
                                                                    <input type="text" id="on_company" name="on_company" class="form-control" required>
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                <div class="form-group">
                                                                    <div class="input-group-addon"><i class="">Language Needed</i></div>
                                                                  
                                                                    <input type="text" id="on_language_needed" name="on_language_needed" class="form-control input-sm valida-texto"  required/>
                                                                    </div>
                                                                </div>
                                                                
                                                                  <div class="col-xs-12 col-md-5">
                                                                    <div class="form-group">
                                                                          <div class="input-group-addon"><i class="">Place/Job Location Address</i></div>
                                                                          <textarea id="on_location_address" name="on_location_address"></textarea>
                                                                        </div>
                                                                    </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--city state-->
                                             
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-4">
                                                              <div class="form-group" >
                                                                <div class="input-group-addon"><i class="">City</i></div>
                                                                <input type="text" id="on_city" name="on_city" class="form-control input-sm valida-texto" required/>
                                                              </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4">
                                                              <div class="form-group">
                                                                  <div class="input-group-addon"><i class="">State</i></div>
                                                                  <input type="text" id="on_state" name="on_state" class="form-control input-sm valida-texto" required/>
                                                                    
                                                                  </div>
                                                              </div>
                                                              <div class="col-xs-12 col-md-4">
                                                                    <div class="form-group">
                                                                        <div class="input-group-addon"><i class="">Zipcode</i></div>
                                                                        <input type="text" id="on_zipcode" name="on_zipcode" class="form-control input-sm valida-texto" required/>
                                                                      </div>
                                                                </div>
                                                              
                                                        </div>
                                                    </div>
                                                </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                             
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-4">
                                                              <div class="form-group" >
                                                                <div class="input-group-addon"><i class="">Date the Interpreter is Needed</i></div>
                                                                <input type="date" id="on_date" name="on_date"  class="form-control " placeholder="" required/>
                                                              </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4">
                                                              <div class="form-group">
                                                                  <div class="input-group-addon"><i class="">Hours</i></div>
                                                                  <input style="text-align: center;" type="time" id="on_time" name="on_time" class="form-control " placeholder="" required/>
                                                                    
                                                                  </div>
                                                              </div>
                                                              <div class="col-xs-12 col-md-4">
                                                                    <div class="form-group">
                                                                        <div class="input-group-addon"><i class="">Phone Number</i></div>
                                                                        <input type="number" id="on_phone_number2" name="on_phone_number2" class="form-control input-sm valida-texto"  required/>
                                                                        </div>
                                                                    </div>
                                                              </div>
                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-xs-12 col-md-3">
                                                                  <div class="form-group" >
                                                                    <div class="input-group-addon"><i class="">Record/Claim N&deg;</i></div>
                                                                    <input type="number" id="on_record_claim" name="on_record_claim" class="form-control input-sm valida-texto"  required/>
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                <div class="form-group">
                                                                    <div class="input-group-addon"><i class="">Interpreter Gender Preference</i></div>
                                                                  
                                                                      <select name="on_gender" id="on_gender" style="padding: 10px; background-color: white; color: rgba(0, 0, 0, 0.555);">
                                                                          <option value="">Select Option</option>
                                                                          <option value="Woman" name="Woman">Woman</option>
                                                                          <option value="Man" name="Man">Man</option>
                                                                      </select>
                                                                    </div>
                                                                </div>
                                                                
                                                                  <div class="col-xs-12 col-md-5">
                                                                    <div class="form-group">
                                                                          <div class="input-group-addon"><i class="">Special Instructions/Notes</i></div>
                                                                            <textarea id="on_notes" name="on_notes"></textarea>
                                                                            <input type="hidden" id="on_service_requested" name="service_requested" class="form-control input-sm valida-texto"  value="On-Site Interpretation"/>
                                                                        </div>
                                                                    </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                               
                                                <div class="form-group mt-4">
                                                <!--send-->
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-4"></div>
                                                            <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group" style="text-align: center;">
                                                                  <input class="btn btn-primary" type="submit" value="SEND" style="color:white; background-color: #5d5dff; border-color: #0407c9; font-size: 15px;  padding: 15px;" placeholder="   SEND   "> 
                                                                  </div> 
                                                              </div>
                                                              <div class="col-xs-12 col-md-4"></div>
                                                        </div>
                                                    </div>
                                                </div>
             

                                       </form>
                                </div>
                                
                              </div>
                               
                          </div>
                        </div>              
                    </section>         
                </div>
                </section>
            </div>
      </section>
</div>
<script type="text/javascript">

    function ShowSelected3()
    {
    /* Para obtener el valor */
    var cod = document.getElementById("on_customer").value;
    //alert(cod);
    
    /* Para obtener el texto */
    var combo = document.getElementById("on_customer");
    var selected = combo.options[combo.selectedIndex].text;
    //alert(selected);
    if(cod == 'Yes'){
      $('#po_number3').show(1000);
    }

  }

</script>