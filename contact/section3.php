<style>

#po_number4{
  
  display:none
}
</style>


<div class="tab-pane" id="tab3" role="tabpanel" aria-labelledby="three-tab">
    
    <div class="content">
    <!-- Main content -->
      <section id="form3"  name="form3" method="post" class="wrap"> 
            <div class="row">
                <section class="col-xs-12 col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div>
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item" style="background-color: white; color: white;">
                                      <a id="four-tab" class="nav-link" data-toggle="tab" href="#section3" role="tab" aria-controls="false"><i class="fa fa-document"></i>Request Service</a>
                                    </li>
                                    
                                </ul>
                                <div class="tab-content">
                                  <div class="tab-pane active" role="tabpanel"  id="section3">
                                      <form  method="post" id="form-2" name="form-2" role="form" action="mail/telephonic.php">
                                            <div class="form-group mt-4">
                                                <!--Tipo de servicio-->
                                                <div class="col-xs-12 col-md-12">
                                                    <div>
                                                        <h4 class="text-center text-justify"  style=" background-color: #0407c9; color: white;  visibility: visible; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transition: opacity 1s cubic-bezier(0.5, 0, 0, 1) 0s, transform 1s cubic-bezier(0.5, 0, 0, 1) 0s;">Telephonic Interpretation: Request a Service</h4> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mt-4">
                                                <!--customer-->
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-4"></div>
                                                        <div class="col-xs-12 col-md-4">
                                                              <div class="form-group" style="text-align: center;">
                                                                  <h4><label class="control-label" for="record">Are you an existing customer?</label></h4>
                                                                      <select name="tel_customer" id="tel_customer" onchange="ShowSelected4();" style="padding: 10px; background-color: white; color: rgba(0, 0, 0, 0.555);" required>
                                                                          <option >Select Option</option>
                                                                          <option value="Yes">Yes</option>
                                                                          <option value="No">No</option>
                                                                      </select>
                                                              </div> 
                                                          </div>
                                                          <div class="col-xs-12 col-md-4"></div>
                                                    </div>
                                                </div>
                                            </div>

                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-xs-12 col-md-4" id="po_number4">
                                                                    <div class="form-group" >
                                                                      <div class="input-group-addon"><i class="">PO Number</i></div>
                                                                      <input type="text" id="tel_po_number" name="tel_po_number" class="form-control input-sm valida-texto"  />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Name</i></div>
                                                                        <input type="text" id="tel_first_name" name="tel_first_name" class="form-control input-sm valida-texto"  required/>
                                                                      </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Last Name</i></div>
                                                                      <input type="text" id="tel_last_name" name="tel_last_name" class="form-control input-sm valida-texto"  required/>
                                                                      </div>
                                                                  </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-xs-12 col-md-4">
                                                                    <div class="form-group" >
                                                                      <div class="input-group-addon"><i class="">Contact Number</i></div>
                                                                      <input type="number" id="tel_contact_number" name="tel_contact_number" class="form-control input-sm valida-texto"  required/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Ext</i></div>
                                                                      <input type="number" id="tel_exp_phone" name="tel_exp_phone" class="form-control input-sm valida-texto"  required/>
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group">
                                                                      <div class="input-group-addon"><i class="">Email</i></div>
                                                                      <input type="email" id="tel_email" name="tel_email" class="form-control" placeholder="email@gmail.com"  title=" '@'' (.)" required>
                                                                      </div>
                                                                  </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                             
                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                            
                                                                <div class="col-xs-12 col-md-6">
                                                                  <div class="form-group" >
                                                                    <div class="input-group-addon"><i class="">Company Name</i></div>
                                                                    <input type="text" id="tel_company" name="tel_company" class="form-control" required>
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-6">
                                                                <div class="form-group">
                                                                    <div class="input-group-addon"><i class="">Language Needed</i></div>
                                                                  
                                                                    <input type="text" id="tel_language_needed" name="tel_language_needed" class="form-control input-sm valida-texto"  required/>
                                                                    </div>
                                                                </div>
                                                                

                                                          </div>

                                                    </div>
                                              </div>
                                             
                                              <div class="form-group"> <!--Po number, name last name-->
                                             
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                        <div class="col-md-12">
                                                                    <p class="text-center text-justify text-black"> Date and Time the Service is Needed</p>
                                                                </div>
                                                            <div class="col-xs-12 col-md-6">
                                                              <div class="form-group" >
                                                                <div class="input-group-addon"><i class="">Date the Interpreter is Needed</i></div>
                                                                <input type="date" id="tel_date" name="tel_date" style="text-align: center;" class="form-control " placeholder="" required/>
                                                              </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-6">
                                                              <div class="form-group">
                                                                  <div class="input-group-addon"><i class="">Hours</i></div>
                                                                  <input style="text-align: center;" type="time" id="tel_time" name="tel_time" class="form-control " placeholder="" required/>
                                                                    
                                                                  </div>
                                                              </div>
                                                            
                                                    </div>
                                              </div>
                                              <div class="form-group"> <!--city state-->
                                             
                                             <div class="col-xs-12 col-md-12">
                                                 <div class="row">
                                                     <div class="col-xs-12 col-md-4">
                                                       <div class="form-group" >
                                                         <div class="input-group-addon"><i class="">LEP Name</i></div>
                                                         <input type="text" id="tel_lep_name" name="tel_lep_name" class="form-control input-sm valida-texto" required/>
                                                       </div>
                                                     </div>
                                                     <div class="col-xs-12 col-md-4">
                                                       <div class="form-group">
                                                           <div class="input-group-addon"><i class="">LEP Phone Number</i></div>
                                                           <input type="number" id="tel_lep_phone" name="tel_lep_phone" class="form-control input-sm valida-texto" required/>
                                                             
                                                           </div>
                                                       </div>
                                                       <div class="col-xs-12 col-md-4">
                                                             <div class="form-group">
                                                                 <div class="input-group-addon"><i class="">Is a 3-way call needed?</i></div>
                                                                 <select name="tel_call" id="tel_call" style="padding: 10px;">
                                
                                                                      <option value="Yes" name="Yes" id="Yes">Yes</option>
                                                                      <option value="No" name="No" id="No">No</option>
                                                                  </select>
                                                               </div>
                                                         </div>
                                                       
                                                 </div>
                                             </div>
                                         </div>
                                              <div class="form-group"> <!--Po number, name last name-->
                                                    <div class="col-xs-12 col-md-12">
                                                          <div class="row">
                                                                <div class="col-xs-12 col-md-5">
                                                                  <div class="form-group" >
                                                                    <div class="input-group-addon"><i class="">Do you need the interpreter to initiate the call?</i></div>
                                                                    <select name="tel_initial" id="tel_initial" style="padding: 10px;">
                                
                                                                        <option value="Yes" name="Yes" id="Yes">Yes</option>
                                                                        <option value="No" name="No" id="No">No</option>
                                                                    </select>
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-2">
                                                                <div class="form-group">
                                                                    <div class="input-group-addon"><i class="">Interpreter Gender Preference</i></div>
                                                                  
                                                                      <select name="tel_gender" id="tel_gender" style="padding: 10px; background-color: white; color: rgba(0, 0, 0, 0.555);">
                                                                          <option value="">Select Option</option>
                                                                          <option value="Woman" name="Woman">Woman</option>
                                                                          <option value="Man" name="Man">Man</option>
                                                                          <option value="It doesn’t matter">It doesn’t matter</option>
                                                                      </select>
                                                                    </div>
                                                                </div>
                                                                
                                                                  <div class="col-xs-12 col-md-5">
                                                                    <div class="form-group">
                                                                          <div class="input-group-addon"><i class="">Special Instructions/Notes</i></div>
                                                                          <textarea id="tel_notes" name="tel_notes"></textarea>
                                                                            
                                                                        </div>
                                                                    </div>

                                                                
                                                          </div>

                                                    </div>
                                              </div>
                                               
                                                <div class="form-group mt-4">
                                                <!--send-->
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-4"></div>
                                                            <div class="col-xs-12 col-md-4">
                                                                  <div class="form-group" style="text-align: center;">
                                                                  <input class="btn btn-primary" type="submit" value="SEND" style="color:white; background-color: #5d5dff; border-color: #0407c9; font-size: 15px;  padding: 15px;" placeholder="   SEND   "> 
                                                                  </div> 
                                                              </div>
                                                              <div class="col-xs-12 col-md-4"></div>
                                                        </div>
                                                    </div>
                                                </div>
             

                                       </form>
                                </div>
                                
                              </div>
                               
                          </div>
                        </div>              
                    </section>         
                </div>
                </section>
            </div>
      </section>
</div>
<script type="text/javascript">

    function ShowSelected4()
    {
    /* Para obtener el valor */
    var cod = document.getElementById("tel_customer").value;
    //alert(cod);
    
    /* Para obtener el texto */
    var combo = document.getElementById("tel_customer");
    var selected = combo.options[combo.selectedIndex].text;
    //alert(selected);
    if(cod == 'Yes'){
      $('#po_number4').show(1000);
    }

  }

</script>