  <!--   Big container   -->
  <div class="container">
        <div class="row">
        <div class="col-sm-12 col-sm-offset-2">

            <!--      Wizard container        -->
            <div class="wizard-container">

                <div class="card" data-color="orange" id="wizardProfile">
                    <form action="" method="">
                <!--        You can switch ' data-color="orange" '  with one of the next bright colors: "blue", "green", "orange", "red"          -->

                    	<div class="wizard-header">
                        	
                    	</div> 

						<div>
                            <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin: 0;" >
                                <li class="nav-item" style="background-color: white; color: white;">
                                <a class="nav-link" id="one-tab" data-toggle="tab" href="#tab1" style="font-size:14px;" role="tab" aria-controls="one" aria-selected="false"><span class="fa fab fa-file-alt"></span><span>Document Translation</span></a>
                                </li>
                                <li class="nav-item" style="background-color: white; color: white;">
                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#tab2" style="font-size:14px;" role="tab" aria-controls="two" aria-selected="false"><span class="fa fab fa-users"></span><span>On-Site Interpretation</span></a>
                                </li>
                                <li class="nav-item" style="background-color: white; color: white;">
                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#tab3" style="font-size:14px;" role="tab" aria-controls="three" aria-selected="false"><span class="fa fa-headset"></span><span>Telephonic Interpretation</span></a>
                                </li>
                                <li class="nav-item" style="background-color: white; color: white;">
                                <a class="nav-link" id="four-tab" data-toggle="tab" href="#tab4" style="font-size:14px;" role="tab" aria-controls="four" aria-selected="false"><span class="fa fa-video"></span><span>Video Remote</span></a>
                                </li>
                                <li class="nav-item" style="background-color: white; color: white;">
                                <a class="nav-link" id="five-tab" data-toggle="tab" href="#tab5" style="font-size:14px;"role="tab" aria-controls="five" aria-selected="false"><span class="fas fa-calendar-check"></span><span>Interpreter Training</span></a>
                                </li>
                                <li class="nav-item" style="background-color: white; color: white;">
                                <a class="nav-link" id="six-tab" data-toggle="tab" href="#tab6" style="font-size:14px;" role="tab" aria-controls="six" aria-selected="false"><span class="fas fa-handshake"></span><span>Be part of Our Network</span></a>
                                </li>
                            </ul>

                        </div>
                   

                        <div class="tab-content" id="myTabContent">
                        <div class="tab-pane active" id="tab1" role="tabpanel" aria-labelledby="one-tab">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="info-text text-center text-justify"> Document Translation </h4>
                                        
                                    </div>
                                    <div class="col-12">
                                        
                                        <?php require_once("contact/section1.php") ?>
                                        
                                    </div>
                                    
                                    
                                    
                                </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                                     <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="info-text text-center text-justify"> On-Site Interpretation </h4>
                                        </div>
                                        <div class="col-12">
                                            <?php require_once("contact/section2.php") ?>
                                        </div>
                                    </div>
                            </div><!-- tab2 -->

                            <div class="tab-pane" id="tab3">
                                     <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="info-text text-center text-justify"> Telephonic Interpretation </h4>
                                        </div>
                                        <div class="col-12">
                                            <?php require_once("contact/section3.php") ?>
                                        </div>
                                    </div>
                            </div><!-- tab3 -->
                            <div class="tab-pane" id="tab4">
                                     <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="info-text text-center text-justify"> Video Remote Interpretation </h4>
                                        </div>
                                        <div class="col-12">
                                            <?php require_once("contact/section4.php") ?>
                                        </div>
                                    </div>
                            </div><!-- tab4-->
                            <div class="tab-pane" id="tab5">
                                     <div class="row">
                                       
                                        <div class="col-12">
                                            <?php require_once("contact/section5.php") ?>
                                        </div>
                                    </div>
                            </div><!-- tab5 -->
                            <div class="tab-pane" id="tab6">
                                     <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="info-text text-center text-justify"> Want to become part of our interpreter network?</h4>
                                            <div class="m-4 text-center text-justify">
                                                <p style="color:#000000">We are glad to see you, Welcome. Before we start the process of enrolling you as part of our interpreter network, please answer the following question:</p>
                                            </div>
                                           
                                        </div>
                                        <div class="col-12">
                                            <?php require_once("contact/section6.php") ?>
                                        </div>
                                    </div>
                            </div><!-- tab6 -->
                           
                        </div>
                       
                    </form>
                </div>
            </div> <!-- wizard container -->
        </div>
        </div><!-- end row -->
    </div> <!-- end row -->
    </div><!-- end row -->
    </div> <!-- end row -->