<!DOCTYPE html>
<html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--Boostrap-css-->
        <link rel="stylesheet" href="css/animation-aos.css">
        <link rel="stylesheet prefetch" href="css/aos.css" type="text/css" media="all">
        <!-- Animation-->
        <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
        <link rel="stylesheet" href="css/form.css" type="text/css" media="all">
        <!--Style css-->
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/main.css" type="text/css" media="all">
   
        <title>Beacon Link</title>
        <!-- Font Awesome Icons-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <!-- Google Fonts-->
        <link rel="dns-prefetch" href="//www.google.com">
        <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

        <!-- Plugin CSS-->
        <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

        <!-- Theme CSS - Includes Bootstrap-->
        <link href="scss/navbar.scss" rel="stylesheet">
        <link href="css/creative.min.css" rel="stylesheet"> 
        <link href="cart/assets/sticky-footer-navbar.css" rel="stylesheet">
        <link href="cart/assets/cart.css" rel="stylesheet">

        <!--Video modal-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <script src="js/scripts.js"></script>

        <!--Carousel-->
        <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <script src="https://js.stripe.com/v3/"></script>
      </head>
      <body>
        <!-- Navigation-->
        <nav  class="navbar navbar-expand-lg navbar-light fixed-top py-3" style="background-image: linear-gradient(to right bottom, #0f5e92, #23acf3);">
          <div class="container">
          <a class="navbar-brand"  href="index.php"><img src="img/logo.png" class="logo"></a>
              <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto my-2 my-lg-0 align-content-end" >
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger"  href="index.php">Home</a>
                </li>
                
                <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" target="_blank" href="https://hermes.beacon-link.com/login">Sign In</a>
                </li>
                
              </ul>
            </div>
          
          </div>
        </nav>

        <!-- Navigation -->
        <!-- Content -->
    
          <div class="container-fluid" style="background: linear-gradient(to left bottom, #0f5e92, #23acf3)">
                <div class="container">
                   
                    <div >
                      <div class="sections">
                        <h1 class="h2 text-center text-white font-weight-bold">Thank you for reaching out to us. In order to better help you, tell us what services you are looking for?</h1>
                        </div>
                        <p style="color: white;" class="sub-tittle text-center mt-3">Select one to send us a service request or to obtain information about the service.</p>
                      </div>
                      <?php require_once("contact/form_contact.php") ?>
                  
                    </div>
                </div>
          </div>
        <!-- Content -->
        <!-- Footer -->
        <footer class="footer py-5" style="margin-top:0rem;">
          
            <div class="container py-xl-4 py-lg-3">
              <div class="address row mb-4">
                <div class="col-lg-4 address-grid">
                <div class="row address-info">
                  <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
                  <i class="fa fa-envelope"></i>
                </div>
                <div class="col-md-9 col-8 address-right">
                  <p>
                    <a href="mailto:info@beacon-link.com" class="text-light"> info@beacon-link.com</a>
                  </p>
                </div>

              </div>
            </div>
                  <div class="col-lg-4 address-grid my-lg-0 my-4">
                    <div class="row address-info">
                      <div class="col-md-3 col-4 address-left text-lg-center text-sm-right text-center">
                        <i class="fa fa-phone"></i>
                      </div>
                      <div class="col-md-9 col-8 address-right">
                        <p class="text-light">Language Department<br>(470) 315-4949 EXT 301<br>
                        Media Department <br>(470) 315-4949 EXT 302<br>Toll-Free<br>
                        (844) 706-7388</p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 address-grid">
                  <div class="row address-info">
                  <div class="col-md-2 col-3 address-left text-lg-center text-sm-right text-center">
                            <i class="fa fa-map"></i>
                          </div>
                          <div class="col-md-10 col-9 address-right">
                            <p class="text-light">1755 North Brown Road. Suite 200<br>Lawrenceville, GA 30043</p>
                          </div>
                  </div>
                </div>
              </div>
              <!-- social icons footer -->
              <div class="w3l-footer text-center pt-lg-4 mt-5">
                <ul class="list-unstyled">
                  <li>
                    <a href="https://www.facebook.com/BeaconLink-266797260844967/" target="_blank">
                      <img src="img/fb.png" alt="">
                    </a>
                  </li>
                  <li class="mx-1">
                    <a href="https://www.instagram.com/beaconlinkcomm/" target="_blank">
                      <img src="img/ig.png" alt="">
                    </a>
                  </li>
                  <li class="mx-1">
                    <a href="https://www.youtube.com/channel/UCfGBUWRuLztitQ_ko27YmoA" target="_blank">
                      <img src="img/you.png" alt="">
                    </a>
                  </li>
                </ul>
              </div>
        <!-- copyright -->
        <p style="text-align: right;" class="terms" ><a class="inline cboxElement" href="#inline_content">Terms and Conditions</a></p>
        <p style="text-align: left;" class="copy-right-grids text-light mt-4">© 2020 Beacon Link LLC. All Rights Reserved</p>
        <!-- //copyright -->
      </div>
          
      </footer>
<!-- validador.js -->


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
<script src="js/js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="js/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="js/js/gsdk-bootstrap-wizard.js"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="js/js/jquery.validate.min.js"></script>


<!--calendar -->


</body>

</html>